#!/usr/bin/bash
#
#  make_rootfs
#
# Created by Danny Goossen on 28/12/2017.
#
# MIT License
#
# Copyright (c) 2017 oc-runner, Gioxa Ltd.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#



echo -e "\033[36;1m ++++++++++++++++++++++++++++++++++++\n"
echo -e "    Create rootfs from rpm-distro\n"
echo -e " ++++++++++++++++++++++++++++++++++++\n\033[0;m"

if [ -z "$BASE" ]; then
  BASE=$PWD
fi

. $BASE/trap_print.sh
f_quiet_command

if [ -z "$target" ]; then
  target=$PWD/rootfs
fi
export BASE
export target

mkdir -pv "$target"
mkdir -pv "${BASE}/cache"

if [ ! -d "/${target}" ]; then
  echo -e "\033[35m target dir: \'$target\' not found!!!\n\n!!! define \'\$target\' as an absolute path!!!\n\033[0;m\n\n"
  f_fail
fi
rm -fr $target/*

if [ ! -f "$OS_CONFIG" ]; then
  echo -e "\033[35m Define OS_CONFIG variable pointing to an existing \'make_os.conf\' in current dir, abort!!\n\033[0;m\n"
  f_fail
fi

. $OS_CONFIG

if [ -z "$basearch" ]; then
  basearch=x86_64
  echo -e "\033[33m Using make_os default basearch=x86_64.\033[0;m\n"
fi

if [ -z "$yum_config" ]; then
  yum_config=$BASE/yum.conf
fi
if [ ! -f "$yum_config" ]; then
  cat > "$yum_config" <<'EOF'
[main]
cachedir=/var/cache/yum/$basearch/$releasever
keepcache=1
debuglevel=1
logfile=/yum.log
exactarch=1
obsoletes=1
gpgcheck=1
plugins=1
installonly_limit=5
distroverpkg=centos-release
reposdir=./yum.repos.d/
metadata_expire=90m
http_caching=all
color=off
EOF

  echo -e "\033[33m Using make_os default yum_config.\033[0;m\n"
fi
if [ ! -f "$yum_config" ]; then
      echo -e "\033[35m un-define yum_config variable, or define one  pointing to an existing \'$$BASE/yum.conf\', abort!!\n\033[0;m\n"
fi

  if [ -d "/usr/sbin/glibc-fake" ]; then
    GLIBC_FAKE=/usr/sbin/glibc-fake
    NEED_GLIBC_PATCH=0
  else
    GLIBC_FAKE=$BASE/glibc
    NEED_GLIBC_PATCH=1
  fi
  export GLIBC_FAKE

  # need to use host sln
  SLND="/usr/sbin/sln"

  if [ -z "$DISTRO_RELEASE" ]; then
      echo -e "\033[33m Using make_os default Release=7.\033[0;m"
      target_release=7
  fi
  echo
  echo " Target release : $DISTRO_RELEASE"

  f_verbose_command
  echo -e "\n\033[36m-------- RPM prepare user db ------\033[0;m"
  fakeroot mkdir -pv $target/var/lib/rpm
  fakeroot rpmdb --initdb --dbpath $target/var/lib/rpm
  f_quiet_command

  echo "%_dbpath $target/var/lib/rpm" | cat > $HOME/.rpmmacros
  echo "%_tmppath /tmp" | cat >> $HOME/.rpmmacros
  f_verbose_command

  echo -e "\n\033[36m-------- verify pub keys ------\033[0;m"
  gpg --quiet --with-fingerprint $BASE/GPG-KEYS/*
  echo -e "\n\033[36m-------- RPM import pub keys ------\033[0;m"
  fakeroot rpm --verbose --import $BASE/GPG-KEYS/*
  rpm -q gpg-pubkey --qf '%{NAME}-%{VERSION}-%{RELEASE}\t%{SUMMARY}\n'


# TODO figure out how to do different archs
#
  echo -e "\n\033[36m-------- prepare yum cache ------\033[0;m"

  rm -rf "$target/var/cache/yum"
  mkdir -pv "$target/var/cache"
  mkdir -pv $BASE/cache/yum
 # overlay trouble fix, don't touch the pakages, increase usage of mem in openshift online
  find $BASE/cache/yum/*/*/*/{*.xml,*.bz2,gen/*} -type f -exec touch {} + || /bin/true
  rm -fv $BASE/cache/yum/*/*/*/gen/*
  ln -svf "$BASE/cache/yum" "$target/var/cache/"

   echo -e "\n\033[36m-------- YUM install packages ------\033[0;m"

  f_quiet_command
 # fool rpm without prefix for db (rpm bug)
  echo "%_dbpath var/lib/rpm" | cat > $HOME/.rpmmacros
  echo "%_tmppath /tmp" | cat >> $HOME/.rpmmacros
  echo "%__transaction_systemd_inhibit %{nil}" | cat >> $HOME/.rpmmacros

# Fakechroot exclude paths
#
    FAKECHROOT_EXCLUDE_PATH="/tmp:/proc:$target:$BASE/packages:$BASE:/usr/sbin/build-locale-archive.rootfs:$SLND:$GLIBC_FAKE"

# Fakechroot command substitutions
#
    FAKECHROOT_CMD_SUBST=":/usr/sbin/glibc_post_upgrade.x86_64=$GLIBC_FAKE/glibc.post.upgrade.x86_64"
    FAKECHROOT_CMD_SUBST+=":/usr/sbin/build-locale-archive=$GLIBC_FAKE/build.locale.archive.wrap"
    FAKECHROOT_CMD_SUBST+=":/sbin/ldconfig=$GLIBC_FAKE/ld.config.wrap"
    FAKECHROOT_CMD_SUBST+=":/usr/bin/env=/usr/bin/env.fakechroot:/usr/bin/mkfifo=/bin/true"
    FAKECHROOT_CMD_SUBST+=":/usr/bin/ischroot=/bin/true:/sbin/insserv=/bin/true:/bin/mount=/bin/true"
    FAKECHROOT_CMD_SUBST+=":/usr/sbin/install-info=/bin/true"
    FAKECHROOT_CMD_SUBST+=":/usr/sbin/sln:$SLND:/sbin/sln=$SLND:sln=$SLND:/usr/bin/ca-legacy=$GLIBC_FAKE/ca-legacy-bi:/bin/ca-legacy=$GLIBC_FAKE/ca-legacy-bi:ca-legacy=$GLIBC_FAKE/ca-legacy-bi"

    export FAKECHROOT_EXCLUDE_PATH
    export FAKECHROOT_CMD_SUBST

    [ -n "$install_packages" ] && INSTALL_PACKAGES="install ${install_packages}"

    export INSTALL_PACKAGES

    f_verbose_command
      $BASE/fake_bin/fakechroot --use-system-libs -- $BASE/fake_bin/fakeroot -- /usr/bin/yum -c "$yum_config" --installroot="$target" --releasever="$DISTRO_RELEASE" -y $INSTALL_PACKAGES "${install_groups}"
  
    unlink "$target/var/cache/yum"

  
    mkdir -pv -m 775 $target/var/cache/yum
    
    echo -e "\n\033[36m-------- /tmp /root /builds /etc/passwd ------\033[0;m"
    mkdir -pv $target/tmp
    chmod -vR 777 $target/tmp
    rm -rf $target/builds
    mkdir -pv -m 775 $target/builds
    if [ -d "$target/etc/skel" ] ; then
      cp -fv /etc/skel/.bash* $target/root
      cp -fv /etc/skel/.bash* $target/builds
      chmod -v 664 $target/builds/.bash*
    fi
    chmod -v 775  $target/etc/passwd

    echo -e "\n\033[36m------- cp glibc-fake ------\033[0;m"

    if [ "$NEED_GLIBC_PATCH" == 1 ]; then
       sed -i 's:$BASE/glibc:/usr/sbin/glibc-fake:g' $BASE/glibc/*
    fi

    # scripts
    mkdir -pv $target/usr/sbin/glibc-fake
    cp -v $GLIBC_FAKE/* $target/usr/sbin/glibc-fake/

    # populate bin
    cp $target/usr/sbin/build-locale-archive $target/usr/sbin/build-locale-archive.rootfs
    chmod 755 $target/usr/sbin/build-locale-archive.rootfs

    echo -e "\n\033[36m-------- cleanup ------\033[0;m"
    # cleanup
    #  locales
    rm -rf "$target"/usr/{{lib,share}/locale,{lib,lib64}/gconv,bin/localedef,sbin/build-locale-archive}

    #  docs and man pages
    rm -rf "$target"/usr/share/{man/*,doc/*,info/*,gnome/help/*}

    #  cracklibp
    rm -rf "$target"/usr/share/cracklib

    #  i18n
    rm -rf "$target"/usr/share/i18n

    #  sln, need it for image builders
    # rm -rf "$target"/sbin/sln

    rm -f $HOME/.rpmmacros

    echo -e "\n\033[36m-------- RPM list packages ------\033[0;m"
    f_quiet_command
    echo "%_dbpath $target/var/lib/rpm" | cat > $HOME/.rpmmacros
    echo "%_tmppath /tmp" | cat >> $HOME/.rpmmacros
    f_verbose_command
    rpm -qa > $target/etc/system_packages
    echo "Installed $(cat  $target/etc/system_packages | wc -w | tr -d '\n') packages. listed at $target/etc/system_packages"
    rm -f $HOME/.rpmmacros
    echo -e "\n\033[36m-------- rewrite absolut symlinks outside $target ------\033[0;m\n"
    
    f_quiet_command
    cd $target
    echo -e "Rewriting symlinks to:\n"
    find . -lname '/*' | while read l ; do
      dest=$(readlink $l);
      [ -d "$dest" ] && unlink $l;
      echo "ln -sfv $(echo "$dest" | sed 's|'$target'/|/|') $l";
    done | bash
    cd $BASE
    f_verbose_command
  

    if [ -n "$postscript" ]; then
      echo -e "\n\033[36m-------- postcript ------\033[0;m\n"
      $postscript
    fi

    trap - DEBUG
    